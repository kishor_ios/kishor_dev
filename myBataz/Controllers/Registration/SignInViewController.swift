//
//  SignInViewController.swift
//  myBataz
//
//  Created by Rengaraj on 15/04/19.
//  Copyright © 2019 Arraign Technologies. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var signInView: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.signInView.layer.borderWidth = 0.9
        self.signInView.layer.borderColor = UIColor.darkGray.cgColor
        self.signInView.layer.cornerRadius = 5.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
