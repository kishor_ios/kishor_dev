//
//  URLConstants.swift
//  myBataz
//
//  Created by Rengaraj on 15/04/19.
//  Copyright © 2019 Arraign Technologies. All rights reserved.
//

import Foundation

class URLConstants: NSObject {
    // 192.168.1.21:8902
    static let BASE_URL = "http://192.168.1.9:8902/v1/"
   
    static let signUpURL = BASE_URL + "authenticate/driver/signup"
}
