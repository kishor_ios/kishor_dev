//
//  WebServices.swift
//  myBataz
//
//  Created by Rengaraj on 15/04/19.
//  Copyright © 2019 Arraign Technologies. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class WebServices: NSObject {
    static let sharedInstances = WebServices()
    func sendGetRequest(Url: String, successHandler: @escaping ([String: AnyObject])-> Void,failureHandler: @escaping ([Error]) -> Void) {
        Alamofire.request(URL.init(string: Url)!, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    print("\n\nAuth request failed with error:\n \(error)")
                }
                failureHandler([error])
                break
            }
        }
    }
    func sendPutRequest(url:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void) {
        Alamofire.request(URL.init(string: url)!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            
            switch response.result {
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                failureHandler([error as Error])
                break
            }
        }
    }
    func sendPostRequest(url:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void) {
        
        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch response.result
            {
                
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
            }
        }
    }
    func sendGetRequestWithHeaders(Url: String,headers:[String: String] ,successHandler: @escaping ([String: AnyObject])-> Void,failureHandler: @escaping ([Error]) -> Void) {
        
        Alamofire.request(URL.init(string: Url)!, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            
            switch response.result
            {
                
            case .success(_):
                
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                if error._code == NSURLErrorTimedOut {
                    //HANDLE TIMEOUT HERE
                    
                    print("\n\nAuth request failed with error:\n \(error)")
                }
                
                failureHandler([error])
                break
                
            }
        }
    }
    func sendPostUrlEncodedRequestWithHeaders(url:String,headers:[String:String] ,parameters:[String:Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void){
        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding:URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch response.result
            {
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
                
            }
        }
    }
    func sendPostRequestWithHeaders(url:String,headers:[String:String] ,parameters:[String:Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void) {
        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result {
            case .success(_):
                if let json = response.result.value {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                failureHandler([error as Error])
                break
            }
        }
    }
    func sendPutRequestWithHeaders(url:String,headers:[String:String] ,parameters:[String:Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void){
        
        print(url)
        Alamofire.request(URL.init(string: url)!, method: .put, parameters: parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch response.result
            {
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
                
            }
        }
    }
    
    func sendDeleteRequestWithParaHeaders(Url: String,headers:[String: String] ,parameters:[String:Any],successHandler: @escaping ([String: AnyObject])-> Void,failureHandler: @escaping ([Error]) -> Void) {
        //
        //        Alamofire.request(URL.init(string: Url)!, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders?headers)
        
        Alamofire.request(URL.init(string: Url)!, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result {
            case .success(_):
                if let json = response.result.value {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    print("\n\nAuth request failed with error:\n \(error)")
                }
                failureHandler([error])
                break
            }
        }
    }
    
    func sendDeleteRequestWithHeaders(Url: String,headers:[String: String] ,successHandler: @escaping ([String: AnyObject])-> Void,failureHandler: @escaping ([Error]) -> Void) {
        Alamofire.request(URL.init(string: Url)!, method: .delete, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result {
            case .success(_):
                if let json = response.result.value {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                if error._code == NSURLErrorTimedOut {
                    print("\n\nAuth request failed with error:\n \(error)")
                }
                failureHandler([error])
                break
            }
        }
    }
    func sendPostRequestWithHeadersParameters(url:String,headers:[String:String] ,parameters:[String:Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void){
        
        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            print((response.response?.statusCode)!)
            print(response.response)
            switch response.result {
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
            }
        }
    }
}

